# php-ci-demo

This is a minimal demo project, that runs apache and php-fpm as
services in a gitlab-ci job.

* This repository contains a single php file,
  [public/index.php](public/index.php), that just shows
  'Hello World!'.
* I created a [custom docker container](docker/apache),
  that passes all php requests from the [public](public)
  directory to php-fpm.
* The [.gitlab-ci.yml](.gitlab-ci.yml) file defines
  a job that uses curl to run the php script.

You can go to [the pipelines](https://gitlab.com/johanv/php-ci-demo/-/pipelines),
to verify if it still works.

Enjoy!


